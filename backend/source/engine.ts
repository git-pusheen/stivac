import * as matter from 'matter-js';
import {Leaderboard, ServerNetworker} from "./index";
import {Vector} from "matter-js";

export class BodyManager {
    static init(engine: matter.Engine, networker: ServerNetworker) {
        this.networker = networker;
        matter.Events.on(engine, 'collisionStart', (e) => {
            const pairs = e.pairs;
            for (let i = 0; i < pairs.length; ++i) {
                if (BodyManager.bodyToCallback[pairs[i].bodyA.id]) {
                    BodyManager.bodyToCallback[pairs[i].bodyA.id](BodyManager.bodyToParent[pairs[i].bodyB.id]);
                }
                if (BodyManager.bodyToCallback[pairs[i].bodyB.id]) {
                    BodyManager.bodyToCallback[pairs[i].bodyB.id](BodyManager.bodyToParent[pairs[i].bodyA.id]);
                }
            }
        });
    }

    static registerBody(body: matter.Body, parent: MultiplayerObject) {
        BodyManager.bodyToParent[body.id] = parent;
        return this.networker.registerDynamicObject(parent);
    }

    static setCollisionCallback(body: matter.Body, collideCallback: Function) {
        BodyManager.bodyToCallback[body.id] = collideCallback;
    }

    private static networker: ServerNetworker;
    private static bodyToCallback: {[index: number]: Function} = {};
    private static bodyToParent: {[index: number]: MultiplayerObject} = {};
}

export class MultiplayerObject {
    constructor(world: matter.World, body: matter.Body) {
        this.body = body;
        this.serverId = BodyManager.registerBody(this.body, this);
        matter.World.add(world, this.body);
    }

    exportProperties() {
        let toExport = {};

        toExport['x'] = this.body.position.x;
        toExport['y'] = this.body.position.y;

        toExport['rotation'] = this.body.angle;

        this.exportPropertiesSpecific(toExport);
        return toExport;
    }

    exportUpdate() {
        let toExport = {};

        toExport['x'] = this.body.position.x;
        toExport['y'] = this.body.position.y;

        toExport['rotation'] = this.body.angle % (2 * Math.PI);
        if (toExport['rotation'] > Math.PI) { 
            toExport['rotation'] -= 2 * Math.PI;
        } else if (toExport['rotation'] < -Math.PI) { 
            toExport['rotation'] += 2 * Math.PI;
        }

        this.exportUpdateSpecific(toExport);
        return toExport;
    }

    destroy(world: matter.World) {
        matter.World.remove(world, this.body);
    }

    update(deltaInSec: number) {}

    shouldBeDeleted() {
        return this.deleteMePls;
    }

    markForDeletion() {
        this.deleteMePls = true;
    }

    getServerId() {
        return this.serverId;
    }

    protected exportUpdateSpecific(toExport) {}

    protected exportPropertiesSpecific(toExport) {}

    body: matter.Body;
    protected deleteMePls: boolean = false;
    private serverId: number;
}

class Shot extends MultiplayerObject {
    constructor(world: matter.World, x: number, y: number, damage: number,
                kick: number, collisionKick : number, shotAngle : number,
                force: number) {
        super(world, matter.Bodies.circle(x, y, 8, { frictionAir: 0 }));

        this.damage = damage;
        this.kick = kick;
        this.collisionKick = collisionKick;
        matter.Body.applyForce(this.body, this.body.position,
            {x: force * Math.cos(shotAngle), y: force * Math.sin(shotAngle)});
    }

    protected exportPropertiesSpecific(toExport) {
        toExport['type'] = 'shot';
    }

    public getKick() {
        return this.kick;
    }

    public getDamage() {
        return this.damage;
    }

    public getCollisionKick() {
        return this.collisionKick;
    }

    private damage: number;
    private kick: number;
    private collisionKick: number;
}

enum BarrelType {
    defaultBarrel,
    sniperBarrel
}

class Barrel {
    constructor(playerID: number, tankPosition: Vector, world: Matter.World) {
        this.playerID = playerID;
        this.tankPosition = tankPosition;
        this.world = world;
        this.resetBarrel();
    }

    public setShotParams(barrelType: BarrelType) {
        switch(barrelType) {
            case BarrelType.defaultBarrel: {
                this.damage = 10;
                this.kick = 0.03;
                this.collisionKick = 4;
                this.force = 0.01;
                this.shotIntervalInMs = 400;
                break;
            }
            case BarrelType.sniperBarrel: {
                this.damage = 30;
                this.kick = 0.08;
                this.collisionKick = 10;
                this.force = 0.08;
                this.shotIntervalInMs = 900;
                break;
            }
        }
        this.barrelType = barrelType;
    }

    public setBarrelAngle(angle: number) {
        this.barrelAngle = angle;
    }

    public getBarrelAngle() {
        return this.barrelAngle;
    }

    public makeShot() {
        const currTime = Date.now();
        if (currTime - this.lastShotTime < this.shotIntervalInMs) {
            return null;
        }
        this.lastShotTime = currTime;

        let x = this.tankPosition.x + 42*Math.cos(this.barrelAngle);
        let y = this.tankPosition.y + 42*Math.sin(this.barrelAngle);
        let shot = new Shot(this.world, x, y, this.damage, this.kick,
            this.collisionKick, this.barrelAngle, this.force);
        BodyManager.setCollisionCallback(shot.body, (object: MultiplayerObject) => {
                if(object instanceof  Tank) {
                    if(object.tryToKill(shot)) {
                        if (object.getServerId() != this.playerID) {
                            Leaderboard.addScore(1, this.playerID);
                        }
                    }
                }
                if(object instanceof Bonus) {
                    object.markForDeletion();
                }
                shot.markForDeletion();
            });
        return shot;
    }

    public resetBarrel() {
        this.setShotParams(BarrelType.defaultBarrel);
    }

    public getBarrelType() {
        return this.barrelType;
    }

    private barrelAngle: number;
    private playerID: number;
    private tankPosition: Vector;
    private damage: number;
    private kick: number;
    private collisionKick: number;
    private force: number;
    private shotIntervalInMs: number;
    private world: Matter.World;
    private barrelType: BarrelType;
    private lastShotTime: number = 0;
}

export enum BonusType {
    healthBonus,
    sniperBonus,
    defaultBonus
}

export class Bonus extends MultiplayerObject{
    constructor(world: matter.World, x: number, y: number, bonusType: BonusType) {
        super(world, matter.Bodies.circle(x, y, 24, { frictionAir: 0 }));
        let angle = Math.floor(Math.random()*(2*Math.PI+1)+(-Math.PI));
        matter.Body.applyForce(this.body, this.body.position,
            {x: 0.1 * Math.cos(angle), y: 0.1 * Math.sin(angle)});
        this.bonusType = bonusType;

        setTimeout(() => {
            this.markForDeletion();
        }, 30000);
    }

    protected exportPropertiesSpecific(toExport) {
        toExport['type'] = 'bonus';
        toExport['bonusType'] = this.bonusType;
    }

    public getBonusType() {
        return this.bonusType;
    }

    private bonusType: BonusType;
}

export class Tank extends MultiplayerObject {
    constructor(world: matter.World, x: number, y: number, nickname: string) {
        super(world, matter.Bodies.circle(x, y, 32, { frictionAir: 0 }));

        this.nickname = nickname;
        this.barrel = new Barrel(this.getServerId(), this.body.position, world);
        console.log('New tank at', this.body.position.x, this.body.position.y);

        BodyManager.setCollisionCallback(this.body, (object: MultiplayerObject) => {
            if(object instanceof  Bonus) {
                switch(object.getBonusType()) {
                    case BonusType.sniperBonus: {
                        this.barrel.setShotParams(BarrelType.sniperBarrel);
                        break;
                    }
                    case BonusType.defaultBonus: {
                        this.barrel.setShotParams(BarrelType.defaultBarrel);
                        break;
                    }
                    case BonusType.healthBonus: {
                        this.health = Math.min(this.health + 30, Tank.maxHealth);
                        break;
                    }
                }
                object.markForDeletion();
            }
        });
    }

    public throttle(mode: string) {
        const oneMoveFuel = 7;
        if (this.fuel < oneMoveFuel) {
            return;
        }
        const fuelBoost = 0.002;
        this.fuel -= oneMoveFuel;
        switch (mode) {
            case 'W':
            matter.Body.applyForce(this.body, this.body.position, {x: 0, y: -fuelBoost});
            break;
            case 'A':
            matter.Body.applyForce(this.body, this.body.position, {x: -fuelBoost, y: 0});
            break;
            case 'S':
            matter.Body.applyForce(this.body, this.body.position, {x: 0, y: fuelBoost});
            break;
            case 'D':
            matter.Body.applyForce(this.body, this.body.position, {x: fuelBoost, y: 0});
            break;
        }
    }

    public update(deltaInSec: number) {
        const regenPerSec = 60;
        if (this.fuel < Tank.maxFuel) {
            this.fuel += regenPerSec * deltaInSec;
        }
    }

    public setBarrelAngle(angle: number) {
        this.barrel.setBarrelAngle(angle);
    }

    protected exportPropertiesSpecific(toExport) {
        toExport['type'] = 'tank';
        toExport['nickname'] = this.nickname;
    }

    protected exportUpdateSpecific(toExport) {
        //console.log('Tank at', this.body.position.x, this.body.position.y);
        toExport['barrelAngle'] = this.barrel.getBarrelAngle();
        toExport['fuel'] = this.fuel;
        toExport['health'] = this.health;
        toExport['barrelType'] = this.barrel.getBarrelType();
    }

    public makeShot() {
        const shot = this.barrel.makeShot();
        if (shot) {
            this.moveUsingShot(shot);
        }
    }

    public tryToKill(shot: Shot) {
        this.health -= shot.getDamage();
        const angle = Math.atan2(this.body.position.y - shot.body.position.y,
                                 this.body.position.x - shot.body.position.x);
        matter.Body.applyForce(this.body, this.body.position,
            {x: shot.getCollisionKick() * Math.cos(angle),
                y: shot.getCollisionKick() * Math.sin(angle)});

        if (this.isKilled()) {
            this.resetTank();
            return true;
        }
        return false;
    }

    private isKilled() {
        return this.health <= 0;
    }

    private resetTank() {
        matter.Body.setPosition(this.body, {x: 100, y: 100});
        matter.Body.setVelocity(this.body, {x: 0, y: 0});
        matter.Body.setAngularVelocity(this.body, 0);
        this.health = Tank.maxHealth;
        this.fuel = Tank.maxFuel;
        this.barrel.resetBarrel();
    }

    public moveUsingShot(shot: Shot) {
        matter.Body.applyForce(this.body, this.body.position,
            {x: -shot.getKick() * Math.cos(this.barrel.getBarrelAngle()),
                y: -shot.getKick() * Math.sin(this.barrel.getBarrelAngle())});
    }

    private static readonly maxFuel: number = 1000;
    private fuel: number = Tank.maxFuel;
    private nickname: string;
    private static readonly maxHealth: number = 100;
    private health: number = Tank.maxHealth;
    private barrel: Barrel;
}

export class Planet extends MultiplayerObject {
    constructor(world: matter.World, x: number, y: number, radius: number, gravity: number) {
        super(world, matter.Bodies.circle(x, y, radius, {isStatic: true}));

        this.radius = radius;
        this.gravity = gravity;

        console.log('New planet at', x, y);
    }

    protected exportPropertiesSpecific(toExport) {
        toExport['type'] = 'planet';
        toExport['gravity'] = this.gravity;
        toExport['radius'] = this.radius;
    }

    public affect(object: matter.Body, deltaInSeconds: number) {
        const extraMultiplier = 0.1;
        const dist = matter.Vector.magnitude(matter.Vector.sub(object.position, this.body.position));
        const angle = matter.Vector.angle(object.position, this.body.position);
        const force = extraMultiplier * this.gravity / (dist * dist) * deltaInSeconds;

        matter.Body.applyForce(object, object.position, matter.Vector.create(force * Math.cos(angle), force * Math.sin(angle)));
        //console.log('Affecting with force', force, 'angle', angle);
    }

    gravity: number;
    radius: number;
}