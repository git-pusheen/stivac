import * as sio from 'socket.io';
import * as matter from 'matter-js';
import * as Engine from './engine';
import {BonusType, MultiplayerObject} from "./engine";

const WORLD_SIDE: number = 1000;

export class ServerNetworker {
    constructor(world: matter.World) {
        this.server = sio.listen(42024);
        this.world = world;
        this.server.on('connection', this.newClient.bind(this));
    }

    private newClient(socket: sio.Socket) {
        socket.on('new', (nickname: string, tankInfoCallback: Function) => {
            const tank = new Engine.Tank(this.world, 100, 100, nickname);

            Leaderboard.addPlayer(nickname, tank.getServerId());

            let tankInfo = tank.exportProperties();
            let tankId = tankInfo['id'] = tank.getServerId();

            socket.on('player', (event: string, eventData) => {
                this.playerEvent(tankId, event, eventData);
            });

            socket.on('disconnect', () => {
                Leaderboard.removePlayer(tankId);
                this.deleteObject(tankId);
            });

            tankInfoCallback(tankInfo);
        });

        let objectsBatch = [];
        for (let objid in this.idToObject) {
            const currObject = this.idToObject[objid] as Engine.MultiplayerObject;
            let toSend = {'data': currObject.exportProperties()};
            toSend['id'] = objid;
            objectsBatch.push(toSend);
        }
        socket.emit('update', {'update': [], 'create': objectsBatch, 'delete': []});
    }

    private playerEvent(tankId: number, event: string, eventData) {
        const tank = this.idToObject[tankId] as Engine.Tank;
        switch (event) {
            case 'barrel':
            tank.setBarrelAngle(eventData);
            break;
            case 'W':
            case 'A':
            case 'S':
            case 'D':
            tank.throttle(event);
            break;
            case 'shot':
            tank.makeShot();
            break;
        }
    }

    public registerDynamicObject(object: Engine.MultiplayerObject): number {
        console.log('New obj registred ', this.globalObjId);
        this.objectsToSendCreation.push(this.globalObjId);
        this.idToObject[this.globalObjId] = object;
        return this.globalObjId++;
    }

    private deleteObject(id: number) {
        (this.idToObject[id] as Engine.MultiplayerObject).destroy(this.world);
        this.objectsToSendDeletion.push(id);
        console.log("deleted");
        delete this.idToObject[id];
    }

    public prepareObjectCreations() {
        let objectsBatch = []; 
        for (let objid of this.objectsToSendCreation) {
            console.log(objid);
            const currObject = this.idToObject[objid];
            if (!currObject) {
                continue;
            }
            let toSend = {'data': currObject.exportProperties()};
            toSend['id'] = objid;
            objectsBatch.push(toSend);
        }
        this.objectsToSendCreation = [];
        return objectsBatch;
    }

    public updateWorldAndSend(deltaInSec: number) {
        let updateObjectsBatch = [];
        for (let objid in this.idToObject) {
            const object = this.idToObject[objid];
            if (object.shouldBeDeleted()) {
                this.deleteObject(Number(objid));
                console.log('del coz asked');
                continue;
            }
            if (!object.body.isStatic) {
                for (const planet of this.planets) {
                    planet.affect(object.body, deltaInSec);
                }
            }
            object.update(deltaInSec);
            let toSend = {'data': object.exportUpdate()};
            toSend['id'] = objid;
            toSend['interval'] = deltaInSec;
            updateObjectsBatch.push(toSend);
        }
        this.server.emit('update', {'update': updateObjectsBatch,
            'create': this.prepareObjectCreations(),
            'delete': this.prepareObjectDeletions()});
    }

    public sendLeaderboard() {
        const scores = Leaderboard.getScores();
        let nickToScore = {};
        for (let key in scores) {
            nickToScore[scores[key].nickname] = scores[key].score;
        }
        this.server.emit('leaderboard', nickToScore);
    }

    public prepareObjectDeletions() {
        let toRet = this.objectsToSendDeletion;
        this.objectsToSendDeletion = [];
        return toRet;
    }

    public registerPlanet(planet: Engine.Planet) {
        this.registerDynamicObject(planet);
        this.planets.push(planet);
    }

    private objectsToSendCreation: number[] = [];
    private objectsToSendDeletion: number[] = [];

    private world: matter.World;
    private idToObject: {[index: number]: Engine.MultiplayerObject} = {};
    private globalObjId: number = 0;
    private server: sio.Server;

    private playerCounter: number = 0;
    private planets: Engine.Planet[] = [];
}

export class LeaderboardEntry {
    constructor(nickname: string, score: number) {
        this.nickname = nickname;
        this.score = score;
    }
    nickname: string;
    score: number;
}

export class Leaderboard {
    public static addPlayer(nickname: string, tankId: number) {
        Leaderboard.scores[tankId] = new LeaderboardEntry(nickname, 0);
    }
    public static removePlayer(tankId: number) {
        delete Leaderboard.scores[tankId];
    }
    public static addScore(score: number, tankId: number) {
        Leaderboard.scores[tankId].score += score;
    }
    public static getScores() {
        return Leaderboard.scores;
    }

    private static scores: {[index: number]: LeaderboardEntry} = {};
}

class Main {
    constructor() {
        this.engine = matter.Engine.create();
        this.engine.world.gravity.y = 0;
        
        this.worldBounds.push(matter.Bodies.rectangle(0, -WORLD_SIDE * 1.5, 4 * WORLD_SIDE, WORLD_SIDE, {isStatic: true}));
        this.worldBounds.push(matter.Bodies.rectangle(0, WORLD_SIDE * 1.5, 4 * WORLD_SIDE, WORLD_SIDE, {isStatic: true}));
        this.worldBounds.push(matter.Bodies.rectangle(-WORLD_SIDE * 1.5, 0, WORLD_SIDE, 4 * WORLD_SIDE, {isStatic: true}));
        this.worldBounds.push(matter.Bodies.rectangle(WORLD_SIDE * 1.5, 0, WORLD_SIDE, 4 * WORLD_SIDE, {isStatic: true}));
        matter.World.add(this.engine.world, this.worldBounds);

        this.network = new ServerNetworker(this.engine.world);
        Engine.BodyManager.init(this.engine, this.network);
        
        this.network.registerPlanet(new Engine.Planet(this.engine.world, 300, 300, 128, 10000));
        this.network.registerPlanet(new Engine.Planet(this.engine.world, 600, 600, 128, 16000));
        this.network.registerPlanet(new Engine.Planet(this.engine.world, -400, -400, 256, 100000));

        setInterval(() => {
            this.network.sendLeaderboard();
        }, 1000);

        const intervalMs: number = 16;
        setInterval(() => {
            this.update(intervalMs);
        }, intervalMs);
        setInterval(() => {
            this.createBonus();
        }, 5000);
    }

    update(deltaInMs: number) {
        matter.Engine.update(this.engine, deltaInMs);
        this.network.updateWorldAndSend(deltaInMs / 1000);
    }

    private createBonus() {
        let bonusType = Math.floor(Math.random() * 3);
        let coordinates = [{x: 0, y: -WORLD_SIDE * 0.9},
                            {x: 0, y: WORLD_SIDE * 0.9},
                            {x: -WORLD_SIDE * 0.9, y: 0},
                            {x: WORLD_SIDE * 0.9, y: 0}];
        let coordInd = Math.floor(Math.random() * 4);
        const bonus = new Engine.Bonus(this.engine.world, coordinates[coordInd].x,
                                                coordinates[coordInd].y, bonusType);
    }

    engine: matter.Engine;
    network: ServerNetworker;
    worldBounds: matter.Body[] = [];
}

new Main();