import * as Phaser from 'phaser';

// no gravity on frontend
export class MultiplayerSprite extends Phaser.Physics.Arcade.Sprite {
    constructor(scene: Phaser.Scene, x: number, y: number, texture: string, staticBody: boolean) {
        super(scene, x, y, texture);
        scene.physics.add.existing(this, staticBody); // add the created body to the world
        this.staticBody = staticBody; // is it static or dynamic (moving)
        this.scene.add.existing(this); // add the object to the scene (world)
    }

    // objectInfo['x'], objectInfo['y'] set outside
    /**
     * Import some properties that are not in constructor
     * @param objectInfo 
     */
    importProperties(objectInfo) {
        this.setRotation(objectInfo['rotation']);
    }

    /**
     * Apply update to the game object a way that it will be in the updated state when next applyUpdate is called
     * @param updateInfo A patch
     * @param duringTimeInSec Time to next applyUpdate call
     */
    applyUpdate(updateInfo, duringTimeInSec: number) {
        if (!this.staticBody) {
            if (this.lastPosX !== undefined) {
                this.x = this.lastPosX; // move the object to the poistion when it should be
                this.y = this.lastPosY;
            }
            this.setVelocity((<number>updateInfo['x'] - this.x) / duringTimeInSec, (<number>updateInfo['y'] - this.y) / duringTimeInSec);
            this.lastPosX = updateInfo['x'];
            this.lastPosY = updateInfo['y'];
        }
        if (this.lastRotation !== undefined) {
            this.setRotation(this.lastRotation);
        }
        let angDiff = (<number>updateInfo['rotation'] - this.rotation); // eh, angle conversion
        if (angDiff > Math.PI) {
            angDiff -= 2 * Math.PI;
        }
        if (angDiff < -Math.PI) {
            angDiff += 2 * Math.PI;
        }
        this.setAngularVelocity((180 / Math.PI) * angDiff / duringTimeInSec);
        this.lastRotation = updateInfo['rotation'];

        this.applyUpdateSpecific(updateInfo, duringTimeInSec); // virtual call, apply specific updates
    }

    /**
     * Some kind of object's desturctor. Clean everything before removal
     */
    cleanBeforeDestroy() {}

    // to be overriden, append/take values to/from the dict

    /**
     * Apply object-specific updates
     * @param updateInfo A patch
     * @param duringTimeInSec Time to next applyUpdate call
     */
    protected applyUpdateSpecific(updateInfo, duringTimeInSec: number) {}

    staticBody: boolean;
    
    lastPosX: number;
    lastPosY: number;
    lastRotation: number;
}

export class Tank extends MultiplayerSprite {
    constructor(scene: Phaser.Scene, x: number, y: number, nickname: string) {
        super(scene, x, y, 'tank', false);

        console.log('New tank', x, y, nickname);
        console.trace();

        this.setCircle(32); // for physics
        this.setDepth(1);

        this.nickname = nickname;
        this.nicknameHolder = this.scene.add.text(x, y, this.nickname); // create nickname holder object
        this.nicknameHolder.setOrigin(0.5, 1);
        this.nicknameHolder.setDepth(2);
        this.barrel = this.scene.add.image(x, y, 'barrel0').setOrigin(0, 0.5)
    }

    /**
     * Update visualization and inner state
     * @param deltaInSec Time passed since last update
     */
    update(deltaInSec: number) {
        //console.log(this.barrelAngle);
        this.barrel.setX(this.x);
        this.barrel.setY(this.y);
        this.nicknameHolder.setX(this.x);
        this.nicknameHolder.setY(this.y - this.body.radius - 4);
        this.barrel.setRotation(this.barrelAngle);
    }

    setBarrelAngle(angle: number) {
        this.barrelAngle = angle;
    }

    getFuel() {
        return this.fuel;
    }

    getHealth() {
        return this.health;
    }

    applyUpdateSpecific(updateInfo, duringTimeInSec: number) {
        if (!updateInfo['localTank']) {
            this.setBarrelAngle(updateInfo['barrelAngle'] as number);
        }
        this.fuel = updateInfo['fuel'];
        this.health = updateInfo['health'];
        if (this.barrelType != updateInfo['barrelType']) {
            this.barrelType = updateInfo['barrelType'];
            this.barrel.destroy();
            this.barrel = this.scene.add.image(this.x, this.y, 'barrel' + this.barrelType).setOrigin(0, 0.5)
        }
    }

    cleanBeforeDestroy() {
        console.log("deleted fron");
        this.barrel.destroy();
        this.nicknameHolder.destroy();
    }

    private fuel: number = 100;
    private health: number = 100;
    private barrelType: number = 0;
    private readonly nickname: string;
    private barrelAngle: number;
    private barrel: Phaser.GameObjects.Image;
    private readonly nicknameHolder: Phaser.GameObjects.Text;
}

export class Planet extends MultiplayerSprite {
    constructor(scene: Phaser.Scene, x: number, y: number, radius: number) {
        super(scene, x, y, 'planet', true);

        console.log('New planet', x, y);

        this.setCircle(radius);
        this.setScale(radius / 128);
        this.setDepth(-1);
    }
}

export enum BonusType {
    healthBonus,
    sniperBonus,
    defaultBonus
}

export class Bonus extends MultiplayerSprite {
    constructor(scene: Phaser.Scene, x: number, y: number, bonusType: number) {
        let bonusImg : string;
        switch (bonusType){
            case BonusType.healthBonus: {
                bonusImg = "healthBonus";
                break;
            }
            case BonusType.sniperBonus: {
                bonusImg = "sniperBonus";
                break;
            }
            case BonusType.defaultBonus: {
                bonusImg = "defaultBonus";
                break;
            }
        }
        super(scene, x, y, bonusImg, false);

        console.log('New bonus', x, y);

        this.setCircle(24);
        this.setDepth(1);
    }
}

export class Shot extends MultiplayerSprite {
    constructor(scene: Phaser.Scene, x: number, y: number) {
        super(scene, x, y, 'shot', false);

        console.log('New shot', x, y);

        this.setCircle(8);
        this.setDepth(1);
    }
}