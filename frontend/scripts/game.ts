import * as Phaser from 'phaser';
import * as Engine from './engine';
import * as sio from 'socket.io-client';
import GameScalePlugin from 'phaser-plugin-game-scale';

const WORLD_SIDE: number = 1000;

class ClientNetworker {
    constructor(scene: Phaser.Scene) {
        this.scene = scene;
        this.server = sio.connect("127.0.0.1:42024");
        this.server.on('update', this.updateObjects.bind(this)); // react on 'update' event with updateObjects function
        this.server.on('leaderboard', (leaderboard) => {
            this.leaderboard = leaderboard;
        })
    }

    /**
     * Reacts on 'update' event, updates the world
     * @param batch From server, consists of 'update', 'create', 'delete' parts.
     */
    private updateObjects(batch) {
        let updatesBatch = batch['update'];
        let createsBatch = batch['create'];
        let deletesBatch = batch['delete'];

        this.createObjects(createsBatch);
        this.deleteObjects(deletesBatch);

        //console.log('update: ' + updatesBatch.length);
        updatesBatch.forEach(objectUpdate => {
            //console.log(objectUpdate);
            const serversideObjectId = objectUpdate['id']; // server id of the object
            const updateDesc = objectUpdate['data']; // data, exported from an object by the server
            const updateIntervalInSec = objectUpdate['interval']; // interval with update is performed (xm, can be stored once at the start)

            if (serversideObjectId == this.thisTankId) {
                updateDesc['localTank'] = true; // mark if this object is the player's tank
            }

            if (serversideObjectId in this.idToObject) {
                //console.log('updating', serversideObjectId);
                (<Engine.MultiplayerSprite>this.idToObject[serversideObjectId]).applyUpdate(updateDesc, updateIntervalInSec);
            }
        });
    }

    /**
     * Responsible for objects creation
     * @param createsBatch From server, consists of 'id' and 'data'
     */
    private createObjects(createsBatch) {
        //console.log('create: ' + createsBatch.length);
        createsBatch.forEach(objectCreate => {
            //console.log(objectCreate);
            const assignToId = objectCreate['id']; // serverside id
            const objectType = objectCreate['data']['type']; // type of the object
            const objectDesc = objectCreate['data']; // data exported from the object on the server

            if (assignToId == this.thisTankId) {
                console.log("not recreated tank " + assignToId + " previus " +this.thisTankId);
                return; // no need to create a tank for the player for the second time, it was created at the start
            }

            let created: Engine.MultiplayerSprite;
            console.log('creating', objectType, objectDesc, 'with id', assignToId);
            switch (objectType) {
                case 'tank':
                    created = new Engine.Tank(this.scene, objectDesc['x'], objectDesc['y'], objectDesc['nickname']);
                break;
                case 'planet':
                    created = new Engine.Planet(this.scene, objectDesc['x'], objectDesc['y'], objectDesc['radius']);
                break;
                case 'shot':
                    created = new Engine.Shot(this.scene, objectDesc['x'], objectDesc['y']);
                break;
                case 'bonus':
                    created = new Engine.Bonus(this.scene, objectDesc['x'], objectDesc['y'], objectDesc['bonusType']);
                break;
            }
            created.importProperties(objectDesc); // import some properties that have no place in a constructor
            this.idToObject[assignToId] = created; // store the object
        });
    }

    /**
     * Responsible for objects deletion
     * @param deletesBatch List of id to delete
     */
    private deleteObjects(deletesBatch) {
        //console.log('delete: ' + deletesBatch.length);
        deletesBatch.forEach(idToDelete => {
            if (idToDelete in this.idToObject) {
                let objAsMps = this.idToObject[idToDelete] as Engine.MultiplayerSprite;
                objAsMps.cleanBeforeDestroy(); // some kind of an object's destructor. For example, delete a barrel.
                objAsMps.destroy(); // Phaser's method. Remove from the world.
                delete this.idToObject[idToDelete]; // delete from the dict
            }
        });
    }

    /**
     * Ask the server to create a tank. This function will use the callback when created
     * @param nickname Player's nickname
     * @param withtankCallback Function which takes created Tank
     */
    createPlayerTank(nickname: string, withtankCallback: Function) {
        this.server.emit('new', nickname, (serverTankInfo) => { // callback with tank's exported info
            //console.log('new tank from', serverTankInfo);
            const tank = new Engine.Tank(this.scene, serverTankInfo['x'], serverTankInfo['y'], nickname);
            tank.importProperties(serverTankInfo); // import additional info
            this.idToObject[serverTankInfo['id']] = tank; // save the tank
            this.thisTankId = serverTankInfo['id'];
            withtankCallback(tank); // done
        });
    }

    /**
     * Send a custom player event
     * @param event Event name
     * @param eventData Any data to send to the server
     */
    sendPlayerEvent(event: string, eventData) {
        //console.log('send: ' + event);
        this.server.emit('player', event, eventData);
    }

    /**
     * Call .update(deltInSec) on every registred game object
     * @param deltaInSec Time passed from previous update
     */
    callUpdatesOnObjects(deltaInSec: number) {
        for (let objectKey in this.idToObject) {
            (this.idToObject[objectKey] as Engine.MultiplayerSprite).update(deltaInSec);
        }
    }

    /**
     * Get the leaderboard as string
     */
    getLeaderboardHTML() {
        const unordered = this.leaderboard;
        let ordered = [];
        for (let key in unordered) {
            ordered.push([unordered[key], key]);
        }
        ordered.sort((a, b) => {
            return b[0] - a[0];
        });
        return '<table><tr><td>#</td><td>Score</td><td>Nickname</td></tr>' + ordered.map((v, i, a) => '<tr><td>' + [i + 1, ...v].join('</td><td>') + '</td>').join('</tr>') + '</table>';
    }

    private scene: Phaser.Scene;
    private idToObject = {};
    private server: SocketIOClient.Socket;

    private leaderboard: {[index: string]: number} = {};

    private thisTankId: number;
}

class MainScene extends Phaser.Scene {
    constructor() {
        super({ key: 'STIV' });
    }

    /**
     * Import textures in key, value format
     */
    preload() {
        this.load.image('background', require('../assets/background.png'));
        this.load.image('planet', require('../assets/planet.png'));
        this.load.image('tank', require('../assets/tank.png'));
        this.load.image('barrel0', require('../assets/barrel0.png'));
        this.load.image('barrel1', require('../assets/barrel1.png'));
        this.load.image('shot', require('../assets/shot.png'));
        this.load.image('healthBonus', require('../assets/bonusHealth.png'));
        this.load.image('sniperBonus', require('../assets/bonusSniper.png'));
        this.load.image('defaultBonus', require('../assets/bonusDefaultBarrel.png'));
    }

    /**
     * Run the game with a nickname from UI
     */
    fromUiGameRunner() {
        this.runGame((document.getElementById('nickname') as HTMLInputElement).value); // pass the nickname
        document.getElementById('overlay').remove(); // remove the overlay
        this.input.on('pointerdown', (_) => {
            this.network.sendPlayerEvent('shot', {});
        })
    }

    /**
     * The world's constructor
     */
    create() {
        this.physics.world.setBounds(-WORLD_SIDE, -WORLD_SIDE,
            WORLD_SIDE * 2, WORLD_SIDE * 2); // world borders that limit the world space
        this.physics.world.gravity.y = 0; // disable gravity

        this.add.line(0, 0, -WORLD_SIDE, -WORLD_SIDE, WORLD_SIDE, -WORLD_SIDE, 0xff0000).setOrigin(0, 0);
        this.add.line(0, 0, -WORLD_SIDE, -WORLD_SIDE, -WORLD_SIDE, WORLD_SIDE, 0xff0000).setOrigin(0, 0);
        this.add.line(0, 0, WORLD_SIDE, WORLD_SIDE, WORLD_SIDE, -WORLD_SIDE, 0xff0000).setOrigin(0, 0);
        this.add.line(0, 0, WORLD_SIDE, WORLD_SIDE, -WORLD_SIDE, WORLD_SIDE, 0xff0000).setOrigin(0, 0);

        this.land = this.add.tileSprite(800 / 2, 600 / 2, 4000, 4000, 'background'); // background stuff
        this.land.setScrollFactor(0);
        this.land.setDepth(-1000); // rendering order. The higher - the closer to the player

        this.network = new ClientNetworker(this); // connect
        document.getElementById('gobutton').onclick = () => { this.fromUiGameRunner(); };
        document.getElementById('nickname').onkeypress = (e) => {
            if (e.key === 'Enter') {
                this.fromUiGameRunner();
            }
        };
    }

    /**
     * World updater, called every frame
     * @param time Total time passde
     * @param deltaInMs Time passed from last update
     */
    update(time: number, deltaInMs: number) {
        this.network.callUpdatesOnObjects(deltaInMs / 1000);

        if (!this.player)
            return; // player is not ready yet

        document.getElementById('health-val').innerText = Math.floor(this.player.getHealth()).toString(); // UI
        document.getElementById('fuel-val').innerText = Math.floor(this.player.getFuel()).toString();
        document.getElementById('leaderboard').innerHTML = this.network.getLeaderboardHTML();

        this.land.tilePositionX = -0.1 * this.cameras.main.scrollX; // background stuff
        this.land.tilePositionY = -0.1 * this.cameras.main.scrollY;

        let playerBarrelAngle = Phaser.Math.Angle.Between(this.player.x, this.player.y,
            this.cameras.main.scrollX + this.input.mousePointer.x,
            this.cameras.main.scrollY + this.input.mousePointer.y); // compute polar angle

        this.network.sendPlayerEvent('barrel', playerBarrelAngle); // send 'barrel' event with the angle

        this.player.setBarrelAngle(playerBarrelAngle);

        if (this.WKey.isDown) {
            this.network.sendPlayerEvent('W', playerBarrelAngle);
        }
        if (this.AKey.isDown) {
            this.network.sendPlayerEvent('A', playerBarrelAngle);
        }
        if (this.SKey.isDown) {
            this.network.sendPlayerEvent('S', playerBarrelAngle);
        }
        if (this.DKey.isDown) {
            this.network.sendPlayerEvent('D', playerBarrelAngle);
        }
    }

    /**
     * Create the player tank
     * @param nickname Player's nickname
     */
    runGame(nickname: string) {
        this.WKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W); // get keys to ask every update
        this.AKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
        this.SKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
        this.DKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);

        this.network.createPlayerTank(nickname, (tank: Engine.Tank) => {
            this.player = tank;
            this.cameras.main.startFollow(this.player); // follow the tank
            this.cameras.main.setDeadzone(256, 256);
        });
    }

    private player: Engine.Tank;

    private land: Phaser.GameObjects.TileSprite;

    private network: ClientNetworker;

    private WKey: Phaser.Input.Keyboard.Key;
    private AKey: Phaser.Input.Keyboard.Key;
    private SKey: Phaser.Input.Keyboard.Key;
    private DKey: Phaser.Input.Keyboard.Key;
}

window.onload = () => {
    let config = {
        type: Phaser.AUTO,
        width: 800,
        height: 600,
        parent: '',
        scene: MainScene,
        physics: {
            default: 'arcade' // use simple physics, just for setVelocity
        },
        plugins: {
            global: [{
              key: 'GameScalePlugin', // to scale the game fullscreen
              plugin: GameScalePlugin,
              mapping: 'gameScale',
              data: {mode:'resize-and-fit'}
            }]
        }
    };
    const game = new Phaser.Game(config);
};
