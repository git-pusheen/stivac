# STIVac: Spherical tanks in vacuum

`dev` scripts can be found in `package.json` files.

## Frontend
`cd frontend`

### Initialization
`npm install` to download all the required packages.

### Development run
`npm run dev` to run parcel serve with the output directory set to `.build` folder.

## Backend
`cd backend`

### Initialization
`npm install` to download all the required packages.

### Development run
`npm run dev` to start typescript compiler with the output directory set to `.build` folder and execute `.build/index.js` with `node`.